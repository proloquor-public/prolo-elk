$pwd = Get-Location
$pwd = "$pwd\target"
$ElasticsearchWinZipUri = "https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.7.0-windows-x86_64.zip"
$ElasticsearchWinPath = "$pwd\elasticsearch-8.7.0-windows-x86_64"
$ElasticsearchWinZip = "$ElasticsearchWinPath.zip"
$CertUtilBat = "$ElasticsearchWinPath\elasticsearch-8.7.0\bin\elasticsearch-certutil.bat"
$CaDir = "$pwd\ca"
$CaZip = "$CaDir.zip"

if(-not(Test-Path -Path $pwd)) {
    New-Item -Path $pwd -ItemType Directory
}
Write-Output "Working from $pwd..."

if (-not(Test-Path $ElasticsearchWinPath)) {
    if (-not(Test-Path -Path $ElasticsearchWinZip)) {
        Write-Output "Downloading $ElasticsearchWinZipUri ..."
        (New-Object Net.WebClient).DownloadFile($ElasticsearchWinZipUri, $ElasticsearchWinZip)
    }
    Write-Output "Expanding $ElasticsearchWinZip ..."
    Expand-Archive -Path $ElasticsearchWinZip -DestinationPath $ElasticsearchWinPath
    Remove-Item $ElasticsearchWinZip
}

if(Test-Path -Path $CaDir) {
    Write-Output "Deleting $CaDir..."
    Remove-Item -Recurse $CaDir
}
if(Test-Path -Path $CaZip) {
    Write-Output "Deleting $CaZip..."
    Remove-Item $CaZip
}

Write-Output "Generating Certificate Authority (CA) files...$CaZip"
Invoke-Expression -Command "$CertUtilBat ca --pem -out $CaZip"
Expand-Archive -Path "$CaZip" -DestinationPath "$CaDir"
Remove-Item "$CaZip"
